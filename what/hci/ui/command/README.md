---
Title: What is a Command Line Interface / CLI?
Subtitle: One Line of Input, One or More Lines of Output
Query: true
---

A *command line interface* or *cli* takes in one line of [standard input](https://duck.com/lite?kae=t&q=standard input) from a programming running in a [REPL](/what/repl/), evaluates the input, and responds by printing out one or more lines to [standard output](https://duck.com/lite?q=standard output) or [standard error](https://duck.com/lite?q=standard error) both of which appear to be separate lines on the [terminal](/reviews/books/termaster/).
