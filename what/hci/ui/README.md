---
Title: What is a User Interface?
Subtitle: The Way You Connect to Something to Use It
Query: true
---

You are a *user* when you use a computer and you connect yourself to the computer through an *interface*. Hence the term *user interface* or *UI*. The two most common references to user interfaces are as follows:

* [Graphic User Interfaces](./graphic/)
* [Terminal User Interfaces](./terminal)
* [Command Line User Interfaces](./command)
* [Conversational User Interfaces](https://duck.com/lite?kae=t&q=Conversational User Interfaces)

The field of [HCI](../) studies different ways humans can interact with devices.
