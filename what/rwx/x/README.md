---
Title: X is for Executing, Exercising, Experiencing
---

:::callout-silly
`sudo chmod +rwx me # give myself full perms` 
:::

"Repetition is the Mother of learning."

Hackers learn best. The word Hacker itself comes from the idea of messing with a system beyond its design parameters to see what it's capable of. The process of hacking is itself a learning process.

Fail faster.

### Discussion Questions

* What does *execute* mean?
* What is *experiential learning*? 
* How would you describe someone learning through experience? 
