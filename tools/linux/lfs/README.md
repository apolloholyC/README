---
Title: Linux File System
Query: true
---

Everything is a file in Unix/Linux, even your mouse, screen, keyboard, and running [processes](/what/process/). A *Linux file system* is a low-level way of managing how all of that gets written to disk, or is handled if it is not *really* something that can be written to disk.

:::co-care
Be careful not to confuse the Linux File System with the [Linux File Hierarchy](/tools/linux/lfh/).
:::

