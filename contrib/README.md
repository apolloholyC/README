---
Title: Donating and Contributing
Subtitle: Helping Out Our Knowledge Development Team
---

Donating and contributing to [RWX.GG]{.spy} can take many forms. Perhaps you want to write content and submit a merge request, maybe you want to help moderate the chat, or perhaps you just want to donate money or other resources to the cause.

## Submitting Content

Anyone is welcome to submit content that is relevant to overall course
and site [goals](/boost/oldboost/goals/). Simply submit a pull request
or [open a ticket](https://gitlab.com/rwx.gg/README/-/issues) with
suggestions or bug fixes that are needed. Please be sure to read the
[style guidelines](guide/) before attempting a content contribution.

## Streaming

Another way to contribute is to do your own streams. Streamers who regularly stream their own interpretation of this content and their working through the course can simple open an issue requesting a link on our [streamers](/live/streamers/) page.

## Donating

Currently the only way to donate is to subscribe to [rwxrob.live](https://rwxrob.live) or send money [directly](https://paypal.me/rwxrob). This may change in the future as our project grows.

## Content Contributors

* [\@zerostheory](https://twitch.tv/zerostheory)
* [\@OGLinuk](https://twitch.tv/OGLinuk)
* [\@elementhttp](https://twitch.tv/elementhttp)
* [\@elremingu](https://twitch.tv/elremingu)
* Jovan Vladislav
* [\@mtheory11dim](https://twitch.tv/mtheory11dim)
* [\@smartrefigerator](https://twitch.tv/smartrefigerator)
* [\@returntodust](https://twitch.tv/returntodust)
* [\@MousePotato](https://twitch.tv/MousePotato)
* [\@unres1gned](https://twitch.tv/unres1gned)
* [\@whitcode](https://twitch.tv/whitcode)

There are others. Let us know if we missed anyone.
