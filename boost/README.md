---
Title: Beginner Boosts
---

Here you'll find links to videos covering beginner technology topics
including recordings of live workthroughs of books and other material.
People can follow along and participate asking questions as they go.
Everyone is encouraged to take notes in [Markdown](/lang/md/) and to ask
questions in the Discord or on the live stream. Maintaining the order is
recommended but certainly not required.

* Autodidactic Habits of a Successful Technologist
* Learning Progressive Web Design
* Learning Pandoc Markdown
* The JavaScript Way
* Learning the Linux Command Line
* Good to Gig: Developer Tools, Services, and Practices
* Head First Go
* Structured Data with gRPC Protobuf
* The Bash Manual Page
* Google Bash Scripting Style Guidelines
* Learn Python the Hard Way
* Docker.com Documentation and Tutorials
* GitLab CI/CD
* Kubernetes.io Documentation and Tutorials
* Head First C
* Mastering Algorithms in C
* Gooligum Embedded Programming, Assembly
* Gooligum Embedded Programming, C
