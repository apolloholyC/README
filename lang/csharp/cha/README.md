# C Sharp (C#)  Coding Challenges

These challenges use the simplified script-like files that begin with a
`csharp` shebang line.

```csharp
#!/usr/bin/csharp

Console.WriteLine ("Hello World!");
```
