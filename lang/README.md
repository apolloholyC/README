---
Title: Recommended First Languages to Learn
Subtitle: Languages That Teach Your Brain How to Code Well
Query: true
---

The following nine languages provide the most personal empowerment and overall knowledge of how languages work so that you can pick up *any* language later more easily.

:::co-tip
Keep in mind that there are other [essential languages] that are not included but are required for most [technology occupations](/jobs/).
:::

------------------------- --------------- ---------------------------------------------------
 Language                  Category       Description
------------------------- --------------- ---------------------------------------------------
 Bash                      Imperative     Default shell language for Linux. Use
                                          from the [command line](/what/hci/ui/command/) or as a
                                          [script](https://duck.com/lite?kae=t&q=script).

 Markdown                  Declarative    Primary knowledge source language developed
                                          by writers, for writers which can be easily 
                                          written with nothing but the simplest text editor
                                          and rendered in any other format with tools like
                                          [Pandoc](/tools/pandoc/).

 [JSON](/lang/data/json/)    Data         Human-*readable* structured data language used for 
                                          inter-applications communication.

 [YAML](/lang/data/yaml/)    Data         Human-*writable* JSON-compatible structured data
                                          language for configuration and database-free
                                          information storage. Used for
                                          [Markdown document front-matter](https://duck.com/lite?kae=t&q=Markdown document front-matter).

 HTML                      Declarative    Web *content* language invented by physicists
                                          requiring coding skills to write.

 CSS                       Declarative    Web *style* language added when HTML started
                                          adding appearance elements instead of content
                                          and structure.

 JavaScript                Imperative,    Web *logical* language that allows imperative
                           Functional     logic (if-then) and event (when-then) handling. 

 Go                        Imperative,    Modern Python, Java and C/C++ replacement for
                           Functional     most work created at Google by "Bell Labs
                                          refugees" including Ken Thompson (the creator of
                                          Unix) and Rob Pike (the co-creator of modern Unicode
                                          standard). Gentle intro to pointers and strict typing.

 C                         Imperative     Mother of all languages, literally. Originally 
                                          created to rewrite the first [Unix](/what/unix/)
                                          operating system. Really only need to learn
                                          enough to understand it and thereby all languages.
------------------------- --------------- ---------------------------------------------------

## Essential Languages

These following languages are so common that basic skill programming any any of them is generally considered essential for any [technology occupation](/jobs/). These include many of the recommended languages but not all.

:::co-tip
Remember that once you have learned all the recommended languages picking up new ones is much easier later.
:::

These are ordered by general level of assumed expectation that you know them. There's plenty to keep you busy learning for a long time but don't feel you have to master them all before applying for work opportunities or joining an open source project.

--------------------
 Markdown
 HTML
 CSS
 JSON
 YAML
 XML
 JavaScript
 Bash
 Python
 Java
 C
 Go
 C#
--------------------

:::co-faq

## You forgot ...?

You will find that this selection and order of languages differs significantly from what most traditional educational organizations --- and even modern bootcamps --- would have you learn. This is by design.

Many of the languages taught to beginners (like Java, Python, and Lisp) are ancient and focus on things like single object inheritance that would get you fired from most jobs as a software developer today. 

It is no secret that these organizations and bootcamps are *not* keeping up. They are teaching ancient languages that few in the industry today would pick for [***greenfield projects***](https://duck.com/lite?kae=t&q=greenfield projects). Sure these [legacy languages](https://duck.com/lite?q=legacy languages) will be around for a very long time, and there will be good jobs maintain systems written in them. But than can be said for COBOL and Fortran as well. That doesn't mean you should choose then as them as your first languages to learn. In fact, it is the reason you *should not* learn them until later.

## Python is best for machine learning!

Actually that is [no longer true.](https://www.rtinsights.com/why-golang-and-not-python-which-language-is-perfect-for-ai/)

:::

## See Also

* [Language Categories](/what/lang/)
