---
Title: What Comes Next?
Subtitle: Ideas for *After* Boosting Your Brain
---

This boost is just to get you started right but you aren't *nearly* done with the immense amount of learning required to work in any of the [target fields](/jobs/). Plan on spending an additional 3000-5000 hours of learning to be able to be a competitive candidate for any of the target occupations.

:::co-tip

## Go to College

The statistics still show that college graduates --- no matter what the college --- regularly make 30% more *at every stage of their careers*. Every case is different and candidates can be hired without a degree, in fact, several Silicon Valley companies have written an affidavit stating that they do *not* require degrees, only demonstrateable skills and knowledge. Still, the safe bet is to get a degree, perhaps from [WGU](https://wgu.edu) or a local community college that does not put you into massive debt. The studies show the degree doesn't matter, only that you have one. It is definitely unfair, but that's just the way it is, for now.

:::

So what comes next? That really depends, but here are the most ubiquitous languages and technologies that didn't make that cut to fit into a 16-week intensive boost (in order of relative, broad value to learn):

   Languages                  Technologies
---------------- -------------------------------------------
 Python                     Databases and SQL
 PowerShell                 GraphQL
 Rust                       Web Framework (Vue)
 C#                         Docker
 C++                        Networking 
 Erlang                     Data Structs and Algorithms
 Java                       Machine Learning
 Assembly                   Embedded Electonics

These will rank in importance differently depending on the occupation you are considering.

