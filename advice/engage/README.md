---
Title: Proactive Proof of Engagement
Subtitle: Chart the Course, Ask to Engage
Query: true
---

> "Captain, there's a unknown debris field ahead. I've locked in a course around it. How would you like to proceed?"
>  
> "Slow to one-quarter impulse and engage."

Yes it's a [Star Trek](https://duck.com/lite?kae=t&q=Star Trek) reference --- specifically [Star Trek Voyager](https://duck.com/lite?kae=t&q=Star Trek Voyager), which everyone knows is the best. But it could just as well have been from [The Orville](https://duck.com/lite?kae=t&q=The Orville) --- or metaphorically from your technology team at work or school. Certainly anyone with military experience will recognize it. 

## Prove Your Not Just Checking Out

While this little exchange might not have an officially name, let's call it practicing *proactive proof of engagement*, a critical way to making progress efficiently while demonstrating your interest, attentiveness, and commitment through proactive preparation for the work at hand --- especially when no one is telling you what to do. 

Those who are engaged ask *which* questions, not *what*. They have researched and prepared one or more thoughtful options and just need the decision maker to pick one, not do all the thinking and research and come up with exactly what they should be doing, a practice sometimes called *micro-management*.

:::co-tip
Be particularly careful not to get put [on the bench](/what/bench/) without even knowing it.
:::

:::co-btw
["Cowboys"](/what/cowboy/) practice *over-zealous* proactivity --- the opposite of not doing anything until you are told. They do whatever they want and *cut the decision maker entirely out of the loop.* Cowboys *usually* get fired, unless they are both heartless and superior, in which case they get promoted and make enemies.
:::

## Creatives Live By This

This proactive practice is often used in the film and other creative industries. In [The Making of the Lord of the Rings](https://duck.com/lite?kae=t&q=The Making of the Lord of the Rings)  or [The Making of Star Wars](https://duck.com/lite?kae=t&q=The Making of Star Wars) you see the directors signing or stamping creations marking their quick approval to proceed with that specific design. That's because the creatives are being *paid* to do the creativity so that the visionaries simply see what's right and mark it. 

## Techies Don't Get It

In the tech world, we often think our work is less than creative, that we should be specifically assigned things and not work beyond them, that we should only do what we are told, that if no one tells us what to do it must mean that we don't have to do anything. 

Often this is the case, but *usually* technologists are expected to practice proactive proof of engagement without anyone explicitly saying so --- particularly when the technologist is an intern, junior, or otherwise new to the organization.

Why?

Because even if they never say so, the techs who can help you --- even those *assigned* to help you --- are already working enormously hard to meet the expectations placed on *them*. (Ironically the hardest workers often seem to be those most likely to think they can help someone else out as well.) Can-you-tell-me-what-to-do people create an enormous drain on a helper's time and psychological well-being because they feel bad for not helping. 

## An Autodidactic Solution

All of this is why becoming the best [autodidact](/what/autodidact/) possible is so critically important. In many ways it completely addresses this dilemma --- especially for beginners. If you already regularly create your own questions and research your own answers --- for anything --- than you need only adapt to your new environment and keep doing the same thing. 

When you aren't sure what you should be working you naturally prepare a list so that next time to speak to your leader you only have to ask *which* is the best thing to focus on. 

Even if none of them are picked you have overwhelming demonstrated that you are, in fact, thinking about the work at hand, that you have *their* priorities and welfare in mind, that you aren't just there for a paycheck, that you believe anything worth doing is worth doing well and with purpose. 

Such people are gold. *Everyone* wants to work with them. 

Become that person.

